module github.com/a8uhnf/betnomi-test

go 1.16

require (
	github.com/jmoiron/sqlx v1.3.4
	github.com/joeshaw/envdecode v0.0.0-20200121155833-099f1fc765bd
	github.com/lib/pq v1.10.0
	github.com/nats-io/nats.go v1.13.0
	github.com/pkg/errors v0.9.1
	github.com/rubenv/sql-migrate v0.0.0-20211023115951-9f02b1e13857
	google.golang.org/genproto v0.0.0-20211129164237-f09f9a12af12
	google.golang.org/grpc v1.42.0
	google.golang.org/protobuf v1.27.1
)
