# BetNomi Test
---

## Steps to run services
---

1. Run postgres
2. Run NATs
3. Build services
4. Build envoy
5. Run envoy
6. Run services


### 1. Run postgres

```
make docker_run_postgres 
```

### 2. Run NATs

```
make docker_run_NATS
```

### 3. Build services

this will generate service binary located in the respective service directory

```
make build_all
```

### 4. Build envoy

this command generate descriptor from the proto `protos` directory to `service/envoy/protos`.


As I'm working on this project on my MacOS, in envoy I've configured to forward traffic to `host.docker.internal`. On linux machine need to change this one

```
make build-envoy
```

### 5. Run envoy

this command will run envoy in `51051` port and forward traffic to `50052` (user service) and `50051` (transaction service) ports 

```
make docker_run_envoy
```

### 6. Create databases

```
CREATE DATABASE user;
CREATE DATABASE transaction;
```

### 7. Run services

to run user service

- set environment variables from the `service/user/.envrc`
```
 ./user
```
it will run the user service in `50052` port

---
to run transaction service

- set environment variables from the `service/transaction/.envrc`
```
 ./transaction
```

it will run the user service in `50051` port



## Postman collection

```
https://www.getpostman.com/collections/706b2899cbd390be553f
```