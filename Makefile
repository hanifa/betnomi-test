SERVICES=\
	transaction \
	user


docker_run_postgres: ## run postgres in docker
	@docker run -d -e "POSTGRES_USER=root" -e "POSTGRES_PASSWORD=123456" -e "POSTGRES_DB=test_db" -p 5432:5432 --name postgres postgres

docker_run_NATS: ## run NATs in docker
	@docker run -d -p 4222:4222 -p 8222:8222 -p 6222:6222 --name nats-server -ti nats:latest

gen-service-proto:
	@[ "${PROTO_PATH}" ] && echo "parsing proto in ${PROTO_PATH}" || ( echo "PROTO_PATH is not set"; exit 1 )
	protoc -I . \
		-I third_party/protos/googleapis \
       --go_out . --go_opt paths=source_relative \
       --go-grpc_out . --go-grpc_opt paths=source_relative \
       $(PROTO_PATH)

gen-descriptor-proto:
	@protoc -I third_party/protos/googleapis -I. --include_imports --include_source_info \
         --descriptor_set_out=service/envoy/protos/descriptor.pb protos/*.proto



docker_run_envoy:
	@docker run -d --name envoy -p 9901:9901 -p 51051:51051 envoy:v1



build-envoy: gen-descriptor-proto
	@docker build -f service/envoy/Dockerfile -t envoy:v1 .



go-generate:
	@go generate ./...

build_all: go-generate
	@for s in $(SERVICES); do echo $$s; cd ./service/$$s; GOOS=$(uname | tr '[:upper:]' '[:lower:]') GOARCH=amd64 go build; cd ../..; done
help:  ## Show help messages for make targets
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-35s\033[0m %s\n", $$1, $$2}'