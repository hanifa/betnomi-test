package database

import (
	"context"
	"log"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	migrate "github.com/rubenv/sql-migrate"
)

type assetFunc func(name string) ([]byte, error)
type assetDirFunc func(name string) ([]string, error)

// DBConnect creates a new database connection.
func DBConnect(ctx context.Context, dbName string, dbURL string,
	asset assetFunc, assetDir assetDirFunc) (*sqlx.DB, error) {
	// Connect to database and test connection integrity.
	db, err := sqlx.Open("postgres", dbURL)
	if err != nil {
		return nil, errors.Wrap(err, "opening "+dbName+" database")
	}
	err = db.Ping()
	if err != nil {
		return nil, errors.Wrap(err, "pinging "+dbName+" database")
	}

	// Limit maximum connections (default is unlimited).
	db.SetMaxOpenConns(10)

	// Run and log database migrations.
	migrations := &migrate.AssetMigrationSource{
		Asset:    asset,
		AssetDir: assetDir,
		Dir:      "migration",
	}
	n, err := migrate.Exec(db.DB, "postgres", migrations, migrate.Up)
	if err != nil {
		log.Fatalf("database migrations failed! %s", err)
	}
	log.Printf("applied new database migrations: %d", n)
	migrationRecords, err := migrate.GetMigrationRecords(db.DB, "postgres")
	if err != nil {
		log.Fatalf("couldn't read back database migration records")
	}
	if len(migrationRecords) == 0 {
		log.Println("no database migrations currently applied")
	} else {
		for _, m := range migrationRecords {
			log.Printf("migration: %v || applied_at: %v || database migration", m.Id, m.AppliedAt)
		}
	}

	return db, nil
}



