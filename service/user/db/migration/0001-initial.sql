-- +migrate Up


CREATE TABLE users
(
    token      VARCHAR(200)         DEFAULT NULL UNIQUE,
    created_at TIMESTAMPTZ NOT NULL DEFAULT now()
);


-- +migrate Down

DROP TABLE users;
