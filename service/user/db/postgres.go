package db

import (
	"context"
	"fmt"
	"math/rand"
	"time"

	"github.com/a8uhnf/betnomi-test/base/database"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

// PGClient is a wrapper for the user database connection.
type PGClient struct {
	DB *sqlx.DB
}

// NewPGClient creates a new item database connection.
func NewPGClient(ctx context.Context, dbURL string) (*PGClient, error) {
	db, err := database.DBConnect(ctx, "user", dbURL, Asset, AssetDir)
	if err != nil {
		return nil, err
	}
	return &PGClient{db}, nil
}

const qInsertUserToken = `INSERT INTO users (token) VALUES(:token)
RETURNING token, created_at
`

const qInsertUserAmount = `INSERT INTO transactions (token, amount) 
VALUES (:token, 1) `

type Transaction struct {
	Token     string    `json:"token" db:"token"`
	Amount    int64     `json:"amount" db:"amount"`
	CreatedAt time.Time `json:"created_at"`
}

func RandomString(n int) string {
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

	s := make([]rune, n)
	for i := range s {
		s[i] = letters[rand.Intn(len(letters))]
	}
	return string(s)
}

func (pg *PGClient) InsertUserToken() (string, error) {
	tx, err := pg.DB.Beginx()
	if err != nil {
		return "", err
	}
	defer func() {
		switch err {
		case nil:
			err = tx.Commit()
		default:
			tx.Rollback()
		}
	}()

	ret := RandomString(10) + fmt.Sprintf("%v", time.Now().Unix())

	v := struct {
		Token string `json:"token"`
	}{
		Token: ret,
	}

	_, err = tx.NamedExec(qInsertUserToken, v)
	if err != nil {
		fmt.Println(err.Error())
		return "", err
	}
	return ret, nil
}
