package db

type DB interface {
	InsertUserToken() (string, error)
}

//go:generate go-bindata -pkg db -o migrations.go migration/...
