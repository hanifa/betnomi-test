package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"

	userpb "github.com/a8uhnf/betnomi-test/service/user/api"
	"github.com/a8uhnf/betnomi-test/service/user/server"
	"github.com/joeshaw/envdecode"
	"google.golang.org/grpc"
)

func main() {
	cfg := server.Config{}
	err := envdecode.StrictDecode(&cfg)
	if err != nil {
		log.Fatalf("failed to process environment variables")
	}
	serverOptions := []grpc.ServerOption{}
	srv := grpc.NewServer(serverOptions...)
	s, err := server.NewServer(&cfg)
	if err != nil {
		log.Fatalf("%v", err)
	}

	userpb.RegisterUserServer(srv, s)

	l, err := net.Listen("tcp", fmt.Sprintf(":%s", cfg.Port))
	if err != nil {
		log.Fatalf("%v", err)
	}
	if err := srv.Serve(l); err != nil {
		log.Fatalf("%v", err)
	}

	stop := make(chan os.Signal)
	signal.Notify(stop, syscall.SIGTERM, syscall.SIGINT)
	<-stop

	log.Println("shuting down server")
}
