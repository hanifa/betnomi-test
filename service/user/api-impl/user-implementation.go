package api_impl

import (
	"context"
	"log"
	"time"

	"github.com/a8uhnf/betnomi-test/service/user/api"
	userpb "github.com/a8uhnf/betnomi-test/service/user/api"
	"github.com/a8uhnf/betnomi-test/service/user/db"
	"github.com/golang/protobuf/proto"
	"github.com/nats-io/nats.go"
)

type UserImplementation struct {
	DB   *db.PGClient
	Conn *nats.Conn
	userpb.UnimplementedUserServer
}

func (u *UserImplementation) Login(ctx context.Context, request *api.LoginRequest) (*api.LoginResponse, error) {

	ret := &api.LoginResponse{}
	token, err := u.DB.InsertUserToken()
	if err != nil {
		return nil, err
	}
	ret.Token = token
	return ret, err
}

func (u *UserImplementation) Balance(ctx context.Context, request *api.BalanceRequest) (*api.BalanceResponse, error) {
	ret := &api.BalanceResponse{}

	// Send the request
	data, err := proto.Marshal(request)
	if err != nil {
		return nil, err
	}
	msg, err := u.Conn.Request("Hello.World", data, 3*time.Second)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	// Use the response
	log.Printf("Reply: %s", msg.Data)
	err = proto.Unmarshal(msg.Data, ret)
	if err != nil {
		log.Println(err)
		return nil, err
	}


	return ret, nil
}
