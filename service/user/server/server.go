package server

import (
	"context"
	"log"
	"os"

	api_impl "github.com/a8uhnf/betnomi-test/service/user/api-impl"
	"github.com/a8uhnf/betnomi-test/service/user/db"
	"github.com/nats-io/nats.go"
)

func NewServer(cfg *Config) (*api_impl.UserImplementation, error) {
	ctx := context.Background()
	d, err := db.NewPGClient(ctx, cfg.DBURL)
	if err != nil {
		return nil, err
	}

	conn, err := nats.Connect(cfg.NATURL)
	if err != nil {
		return nil, err
	}

	userImpl := &api_impl.UserImplementation{
		DB:   d,
		Conn: conn,
	}

	port := os.Getenv("PORT")
	log.Printf("gRPC server starting at port: %s", port)

	return userImpl, nil
}
