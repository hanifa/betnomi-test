package db

import (
	"context"
	"fmt"
	"time"

	"github.com/a8uhnf/betnomi-test/base/database"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

// PGClient is a wrapper for the user database connection.
type PGClient struct {
	DB *sqlx.DB
}

// NewPGClient creates a new item database connection.
func NewPGClient(ctx context.Context, dbURL string) (*PGClient, error) {
	db, err := database.DBConnect(ctx, "transaction", dbURL, Asset, AssetDir)
	if err != nil {
		return nil, err
	}
	return &PGClient{db}, nil
}

const qUpdateUserAmount = `UPDATE transactions SET amount = amount+ :amount WHERE token=:token
RETURNING token, amount, created_at
`

const qInsertUserAmount = `INSERT INTO transactions (token, amount) 
VALUES (:token, 1) `

type Transaction struct {
	Token     string    `json:"token" db:"token"`
	Amount    int64     `json:"amount" db:"amount"`
	CreatedAt time.Time `json:"created_at"`
}

func (pg *PGClient) UpdateUserAmount(token string, amount int64) (*Transaction, error) {
	tx, err := pg.DB.Beginx()
	if err != nil {
		return nil, err
	}
	defer func() {
		switch err {
		case nil:
			err = tx.Commit()
		default:
			tx.Rollback()
		}
	}()
	ret := Transaction{
		Token:  token,
		Amount: amount,
	}

	rows, err := tx.NamedQuery(qUpdateUserAmount, ret)
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	cnt := 0
	for rows.Next() {
		cnt++
		err = rows.Scan(&ret.Token, &ret.Amount, &ret.CreatedAt)
		if err != nil {
			return nil, err
		}
	}
	if cnt == 0 {
		rows, err = tx.NamedQuery(qInsertUserAmount, ret)
		if err != nil {
			return nil, err
		}
		for rows.Next() {
			err = rows.Scan(&ret.Token, &ret.Amount, &ret.CreatedAt)
			if err != nil {
				return nil, err
			}
		}
	}

	return &ret, nil
}

const qGetUserBalancer = `SELECT amount, token FROM transactions WHERE token=$1`

func (pg *PGClient) GetUserBalance(token string) (*Transaction, error) {
	tx, err := pg.DB.Beginx()
	if err != nil {
		return nil, err
	}
	defer func() {
		switch err {
		case nil:
			err = tx.Commit()
		default:
			tx.Rollback()
		}
	}()

	var ret []Transaction

	err = tx.Select(&ret, qGetUserBalancer, token)
	if err != nil {
		return nil, err
	}

	return &ret[0], err
}
