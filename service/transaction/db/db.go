package db

type DB interface {
	UpdateUserAmount(token string, amount int64) (*Transaction, error)
	GetUserBalance(token string) (*Transaction, error)
}

//go:generate go-bindata -pkg db -o migrations.go migration/...
