-- +migrate Up


CREATE TABLE transactions
(
    token      VARCHAR(30)          DEFAULT NULL UNIQUE,
    amount     INTEGER,
    created_at TIMESTAMPTZ NOT NULL DEFAULT now()
);


-- +migrate Down

DROP TABLE transactions;
