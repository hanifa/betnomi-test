package server

import (
	"fmt"
	"log"
	"sync"

	userpb "github.com/a8uhnf/betnomi-test/service/user/api"
	"github.com/golang/protobuf/proto"
	"github.com/nats-io/nats.go"
)

func (t *Server) BalanceResponder() error {

	_, err := t.conn.Subscribe("Hello.World", t.replyWithUserId)
	if err != nil {
		log.Println(err)
		return err
	}

	wg := sync.WaitGroup{}

	wg.Add(1)
	wg.Wait()
	return nil
}
func (t *Server) replyWithUserId(m *nats.Msg) {

	bln := userpb.BalanceRequest{}
	err := proto.Unmarshal(m.Data, &bln)
	if err != nil {
		fmt.Println(err)
		return
	}
	trnx, err := t.DB.GetUserBalance(bln.Token)
	if err != nil {
		log.Println(err)
		return
	}
	resp := userpb.BalanceResponse{
		Token:  trnx.Token,
		Amount: trnx.Amount,
	}

	b, err := proto.Marshal(&resp)
	if err != nil {
		log.Println(err)
		return
	}

	err = t.conn.Publish(m.Reply, b)
	if err != nil {
		log.Println(err)
		return
	}

}
