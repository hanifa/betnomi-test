package server

import (
	"context"
	"log"
	"os"

	api_impl "github.com/a8uhnf/betnomi-test/service/transaction/api-impl"
	"github.com/a8uhnf/betnomi-test/service/transaction/db"
	"github.com/nats-io/nats.go"
)

type Server struct {
	api_impl.TransactionImpl
	conn *nats.Conn
}

func NewServer(cfg *Config) (*Server, error) {
	ctx := context.Background()
	d, err := db.NewPGClient(ctx, cfg.DBURL)
	if err != nil {
		return nil, err
	}

	nc, err := nats.Connect(cfg.NAT_URL)
	if err != nil {
		return nil, err
	}

	transactionImpl := &Server{

	}
	transactionImpl.DB = d
	transactionImpl.conn = nc

	port := os.Getenv("PORT")
	log.Printf("gRPC server starting at port: %s", port)

	return transactionImpl, nil
}
