package server

type Config struct {
	Port    string `env:"PORT,default=8080"`
	DBURL   string `env:"DB_URL"`
	NAT_URL string `env:"NAT_URL,default=nats://localhost:4222"`
}
