package api_impl

import (
	"context"
	"log"

	"github.com/a8uhnf/betnomi-test/service/transaction/api"
	transactionpb "github.com/a8uhnf/betnomi-test/service/transaction/api"
	"github.com/a8uhnf/betnomi-test/service/transaction/db"
)

type TransactionImpl struct {
	DB *db.PGClient
	transactionpb.UnimplementedTransactionServer
}

func (t *TransactionImpl) Up(ctx context.Context, request *api.UpRequest) (*api.UpResponse, error) {
	ret, err := t.DB.UpdateUserAmount(request.Token, 1)

	if err != nil {
		log.Println(err)
		return &api.UpResponse{}, err
	}
	return &api.UpResponse{
		Token:  ret.Token,
		Amount: ret.Amount,
	}, nil
}

func (t *TransactionImpl) Down(ctx context.Context, request *api.DownRequest) (*api.DownResponse, error) {
	ret, err := t.DB.UpdateUserAmount(request.Token, -1)

	if err != nil {
		log.Println(err)
		return &api.DownResponse{}, err
	}
	return &api.DownResponse{
		Token:  ret.Token,
		Amount: ret.Amount,
	}, nil
}
